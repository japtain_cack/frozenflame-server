# Build remco from specific commit
######################################################
FROM golang AS remco

# remco (lightweight configuration management tool) https://github.com/HeavyHorst/remco
RUN go install github.com/HeavyHorst/remco/cmd/remco@latest


# Build Frozen Flame base
######################################################
FROM ubuntu:oracular AS frozenflame-base
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

USER root

ENV DEBIAN_FRONTEND noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV FROZENFLAME_HOME=/home/frozenflame
ENV FROZENFLAME_UID=10000
ENV FROZENFLAME_GUID=10000

## Install system packages
RUN apt-get -y update && apt-get -y upgrade && apt-get -y install \
    curl \
    vim \
    lib32gcc-s1 \
    ca-certificates \
    lib32z1 \
    libc6-dev \
    file

## Create frozenflame user and group
RUN groupadd -g $FROZENFLAME_GUID frozenflame && \
    useradd -l -s /bin/bash -d $FROZENFLAME_HOME -m -u $FROZENFLAME_UID -g frozenflame frozenflame && \
    passwd -d frozenflame

## Install remco
COPY --from=remco /go/bin/remco /usr/local/bin/remco
COPY --chown=frozenflame:root remco /etc/remco
RUN chmod -R 0775 /etc/remco

## Install SteamCMD
RUN curl -sqL "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz" | tar zxvf - -C ${FROZENFLAME_HOME}/ && \
    chmod ugo+x ${FROZENFLAME_HOME}/steamcmd.sh

## Install entrypoint and set home permissions
COPY --chown=frozenflame:frozenflame files/entrypoint.sh ${FROZENFLAME_HOME}/
RUN chmod ugo+x ${FROZENFLAME_HOME}/entrypoint.sh && \
    chown -Rv frozenflame:frozenflame ${FROZENFLAME_HOME}

# Build Frozen Flame image
######################################################
FROM frozenflame-base as frozenflame
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ARG CI_COMMIT_AUTHOR
ARG CI_COMMIT_TIMESTAMP
ARG CI_COMMIT_SHA
ARG CI_COMMIT_TAG
ARG CI_PROJECT_URL

ENV DEBIAN_FRONTEND noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV FROZENFLAME_HOME=/home/frozenflame
ENV FROZENFLAME_UID=10000
ENV FROZENFLAME_GUID=10000
ENV FROZENFLAME_STEAM_VALIDATE="false"
ENV REMCO_HOME=/etc/remco
ENV STEAMAPPID=1348640

LABEL maintainer=$CI_COMMIT_AUTHOR
LABEL author=$CI_COMMIT_AUTHOR
LABEL description="Frozen Flame dedicated server with SteamCMD automatic updates and remco auto-config"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.name="registry.gitlab.com/japtain_cack/frozenflame-server"
LABEL org.label-schema.description="Frozen Flame dedicated server with SteamCMD automatic updates and remco auto-config"
LABEL org.label-schema.url=$CI_PROJECT_URL
LABEL org.label-schema.vcs-url=$CI_PROJECT_URL
LABEL org.label-schema.docker.cmd="docker run -it -d -v /mnt/frozenflame/world1/:/home/frozenflame/server/ -p 28015:28015 -p 28083:28083 registry.gitlab.com/japtain_cack/frozenflame-server"
LABEL org.label-schema.vcs-ref=$CI_COMMIT_SHA
LABEL org.label-schema.version=$CI_COMMIT_TAG
LABEL org.label-schema.build-date=$CI_COMMIT_TIMESTAMP

WORKDIR $FROZENFLAME_HOME
VOLUME "${FROZENFLAME_HOME}/server"
# server ports
EXPOSE 7777/tcp
EXPOSE 7777/udp
# query ports
EXPOSE 27015/tcp
EXPOSE 27015/udp
# unknown
EXPOSE 25575/tcp

USER frozenflame
ENTRYPOINT ["./entrypoint.sh"]

