1. Get the application URL by running these commands:
{{- range .Values.services }}
{{- if contains "NodePort" .type }}
  export NODE_PORT=$(kubectl get --namespace {{ $.Release.Namespace }} -o jsonpath="{.spec.ports[0].nodePort}" services {{ include "frozenflame.fullname" $ }})
  export NODE_IP=$(kubectl get nodes --namespace {{ $.Release.Namespace }} -o jsonpath="{.items[0].status.addresses[0].address}")
  echo http://$NODE_IP:$NODE_PORT
{{- else if contains "LoadBalancer" .type }}

  NOTE: It may take a few minutes for the "{{ .name }}" LoadBalancer IP to be available.
        You can watch the status of by running 'kubectl get --namespace {{ $.Release.Namespace }} svc -w {{ include "frozenflame.fullname" $ }}'

  export SERVICE_IP=$(kubectl get svc --namespace {{ $.Release.Namespace }} {{ include "frozenflame.fullname" $ }} --template "{{"{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}"}}")
  {{- range .ports }}
  echo $SERVICE_IP:{{ .port }}/{{ .protocol }}
  {{- end }}
{{- else if or (contains "ClusterIP" .type) (contains "headless" .type) }}

  export POD_NAME=$(kubectl get pods --namespace {{ $.Release.Namespace }} -l "app.kubernetes.io/name={{ include "frozenflame.name" $ }},app.kubernetes.io/instance={{ $.Release.Name }}" -o jsonpath="{.items[0].metadata.name}")
  export CONTAINER_PORT=$(kubectl get pod --namespace {{ $.Release.Namespace }} $POD_NAME -o jsonpath="{.spec.containers[0].ports[0].containerPort}")

  frozenflame will be available on the following sockets.
  {{- range .ports }}
  "127.0.0.1:{{ .port }}/{{ .protocol }}"
  kubectl --namespace {{ $.Release.Namespace }} port-forward $POD_NAME {{ .port }}:$CONTAINER_PORT
  {{- end }}
{{- end }}
{{- end }}
