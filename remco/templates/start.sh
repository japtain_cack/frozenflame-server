./FrozenFlameServer.sh \
  -MetaGameServerName="{{ getv("/frozenflame/server/name", "Frozen Flame Dedicated Server") }}" \
  -ip="{{ getv("/frozenflame/server/ip", "127.0.0.1") }}" \
  -Port="{{ getv("/frozenflame/server/port", "7777") }}" \
  -queryPort="{{ getv("/frozenflame/server/queryport", "27015") }}" \
{% if getv("/frozenflame/server/noeac", "false") == "true" %}
  -noeac \
{% endif %}
{% if getv("/frozenflame/server/log", "false") == "true" %}
  -log \
{% endif %}
{% if getv("/frozenflame/server/locallogtimes", "false") == "true" %}
  -LOCALLOGTIMES \
{% endif %}
  -RconPort="{{ getv("/frozenflame/server/rconport", "25575") }}" \
  -RconPassword="{{ getv("/frozenflame/server/rconpassword", "Secret123!") }}"
