[![Tag](https://img.shields.io/gitlab/v/tag/japtain_cack/frozenflame-server?style=for-the-badge)](https://gitlab.com/japtain_cack/frozenflame-server/-/tags)
[![Gitlab Pipeline](https://img.shields.io/gitlab/pipeline-status/japtain_cack/frozenflame-server?branch=master&style=for-the-badge)](https://gitlab.com/japtain_cack/frozenflame-server/-/pipelines)
[![Issues](https://img.shields.io/gitlab/issues/open/japtain_cack/frozenflame-server?style=for-the-badge)](https://gitlab.com/japtain_cack/frozenflame-server/-/issues)
[![License](https://img.shields.io/badge/License-CC%20BY--ND%204.0-blue?style=for-the-badge)](https://creativecommons.org/licenses/by-nd/4.0/)


# FrozenFlame Server

## Overview
Deploy and manage a Frozen Flame dedicated server within a Docker container using this streamlined setup. It incorporates steamCMD for automatic server updates and simplifies server management.

## Quick Start
To run the server, execute the following Docker command, substituting `<ENVIRONMENT_VARIABLE>=<VALUE>` with your specific configurations:

```bash
docker run --name frozenflame -it --rm \
  -p 7777:7777 -p 27015:27015 -p 25575:25575 \
  -v /home/$(whoami)/frozenflame:/home/frozenflame/server \
  -e FROZENFLAME_SERVER_NAME="your_server_name" \
  -e FROZENFLAME_STEAM_VALIDATE="false" \
  -e FROZENFLAME_SERVER_NOEAC="true" \
  -e FROZENFLAME_SERVER_LOG="false" \
  -e FROZENFLAME_SERVER_LOCALLOGTIMES="true" \
  -e FROZENFLAME_GAME_BNOMODULECOST="True" \
  -e FROZENFLAME_GAME_SPRINTSTAMINACOST="0.5" \
  registry.gitlab.com/japtain_cack/frozenflame-server:latest
```

## Best Practices
- **Persistent Storage**: Mount the `game` directory externally to avoid data loss.
- **File Permissions**: Set correct permissions using `chown 1000:1000 mount/path`.
- **Configuration**: Customize the `start.sh` script via environment variables. The script resets on container launch.

## Versioning
The CI/CD pipeline tags images using semantic versioning (`major.minor.patch`). Pin at least to the major version to avoid unexpected breaking changes.

## SELinux Context
For mounted volumes, set the SELinux context:

```bash
chcon -Rt svirt_sandbox_file_t /path/to/volume
```

## Additional Commands
- **Stop Containers**: `docker kill $(docker ps -qa); docker rm $(docker ps -qa)`
- **View Logs**: `docker logs -f frozenflame`
- **Server Console**: `docker attach frozenflame` (Detach with `ctrl+p` + `ctrl+q`)
- **Bash Console**: `docker exec -it frozenflame bash`

## Helm Deployment
Clone the repository and customize the Helm chart values for deployment:

```bash
git clone https://gitlab.com/japtain_cack/frozenflame-server.git
cp frozenflame-server/helm_chart/values.yaml frozenflame_config.yaml
helm install -n frozenflame frozenflame-world1 frozenflame-server/helm_chart -f frozenflame_config.yaml
```

## Configuration Files
- **Server Script**: [start.sh](https://gitlab.com/japtain_cack/frozenflame-server/-/blob/master/remco/templates/start.sh)
- **Game Settings**: [Game.ini](https://gitlab.com/japtain_cack/frozenflame-server/-/blob/master/remco/templates/Game.ini)
- **Helm Values**: [values.yaml](https://gitlab.com/japtain_cack/frozenflame-server/-/blob/master/helm_chart/values.yaml)

## Environment Variables
Utilize [Remco](https://github.com/HeavyHorst/remco) for dynamic configuration management. Set variables like `FROZENFLAME_SERVER_IP` to customize your server settings.

### Optional Variables
- **User/Group ID**: Set `FROZENFLAME_UID` and `FROZENFLAME_GUID` as needed.

Remember to keep your configuration files, such as `frozenflame_config.yaml`, version-controlled within your Git repository.
