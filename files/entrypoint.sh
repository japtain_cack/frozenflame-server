#!/usr/bin/env bash

export REMCO_RESOURCE_DIR=${REMCO_HOME}/resources.d
export REMCO_TEMPLATE_DIR=${REMCO_HOME}/templates
export LD_LIBRARY_PATH=./linux64:./linux32:/home/frozenflame/server/linux64:/home/frozenflame/server/linux32:/usr/lib/x86_64-linux-gnu:$LD_LIBRARY_PATH

echo "#####################################"
date
echo

mkdir -p "${FROZENFLAME_HOME}/server/FrozenFlame/Saved/Config/LinuxServer"
chown -Rv frozenflame:frozenflame ${FROZENFLAME_HOME}

if [[ ! -z $FROZENFLAME_STEAM_VALIDATE ]]; then
  if [[ ! "$FROZENFLAME_STEAM_VALIDATE" =~ true|false ]]; then
    echo '[ERROR] FROZENFLAME_STEAM_VALIDATE must be true or false'
    exit 1
  elif [[ "$FROZENFLAME_STEAM_VALIDATE" == true ]]; then
    FROZENFLAME_STEAM_VALIDATE_VALUE="validate"
  else
    FROZENFLAME_STEAM_VALIDATE_VALUE=""
  fi
fi

cat <<EOF> ${FROZENFLAME_HOME}/frozenflame.conf
@ShutdownOnFailedCommand 1
@NoPromptForPassword 1
@sSteamCmdForcePlatformType linux
force_install_dir ${FROZENFLAME_HOME}/server/
login anonymous
app_update ${STEAMAPPID} ${FROZENFLAME_STEAM_VALIDATE_VALUE}
quit
EOF

./steamcmd.sh +runscript ${FROZENFLAME_HOME}/frozenflame.conf

remco

echo
echo "#####################################"
echo starting server...
echo

cd ${FROZENFLAME_HOME}/server/
./start.sh

